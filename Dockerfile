FROM alpine:latest

RUN mkdir -p /hello_world

WORKDIR /hello_world

RUN apk update && \
    apk add --no-cache \
    g++

COPY ./src /hello_world

RUN g++ hello_world.cpp -o hello_world

ENTRYPOINT [ "./hello_world" ]

CMD [ "run"]
