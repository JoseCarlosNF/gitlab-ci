#include <iostream>
#include <string>

using namespace std;

int main(int argc, char** argv)
{
  if (argc > 2)
  {
    cout << "O código suporta os seguintes argumentos:" << endl;
    cout << "test: 0" << endl;
    cout << "run: 0" << endl;
    cout << "test-fail: 1" << endl;
  }

  string arg = argv[1];

  if (arg == "test")
  {
    cout << "\033[1;32mExecutando teste...\033[0m" << endl;
    return EXIT_SUCCESS;
  }

  else if (arg == "run")
  {
    cout << "\033[1;31mExecutando aplicacao...\033[0m" << endl;
    return EXIT_SUCCESS;
  }

  else if (arg == "fail-test")
  {
    cout << "\033[1;33mTeste falhou com sucesso...\033[0m" << endl;
    return EXIT_FAILURE;
  }

  return EXIT_FAILURE;
}
